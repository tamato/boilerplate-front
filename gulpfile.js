var gulp   = require('gulp');

// css
var scss   = require('gulp-sass');
var prefix = require('gulp-autoprefixer');

// js
var browserify = require('browserify');
var watchify = require('watchify');
var babel = require('babelify');

// utils
var maps   = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var plumber = require('gulp-plumber');
var notifier = require("node-notifier");

var defaultTasks = ['scss', 'js'];

var configs = {
	isDev: process.argv.indexOf('--dev') >= 0,
	src: '.',
	dest: './dist'
}

var prefixConfig = { 
	browsers: ['last 10 versions'] 
};

configs.stylesheets = {
	src: configs.src + '/stylesheets',
	entryPoint: configs.src + '/stylesheets/' + (configs.isDev ? 'app-dev.scss' : 'app-prod.scss'),
	watched: configs.src + '/stylesheets/**/*.scss',
	dest: configs.dest + '/stylesheets',
	outputFilename: 'app.min.css',
}

configs.javascripts = {
	src: configs.src + '/javascripts',
	entryPoint: configs.src + '/javascripts/app.js',
	dest: configs.dest + '/javascripts',
	outputFilename: 'app.min.js',
}

gulp.task('scss', function() {
	return gulp.src(configs.stylesheets.entryPoint)
		.pipe(maps.init())
		.pipe(scss({ 
			outputStyle: configs.isDev ? 'expanded' : 'compressed' 
		}).on('error', function(error) {
			console.log(error.messageFormatted);
			notifier.notify({
					title  : "scss error",
					message: error.message,
				})
			this.emit('end'); 
		}))
		.pipe(rename(configs.stylesheets.outputFilename))
		.pipe(maps.write('.'))
		.pipe(gulp.dest(configs.stylesheets.dest));
})

gulp.task('js', function() {
	var bundler = watchify(
		browserify(
			configs.javascripts.entryPoint,
			{ debug: configs.isDev ? true : false }
		)
		.transform(babel)
	);

  function rebundle() {
    bundler.bundle()
      .on('error', function(err) { 
      	console.error(err);
      	notifier.notify({
					title  : "js error",
					message: err.message,
				}); 
      	this.emit('end'); 
      })
      .pipe(source(configs.javascripts.outputFilename))
      .pipe(buffer())
      .pipe(maps.init({loadMaps: true}))
      .pipe(maps.write('.'))
      .pipe(gulp.dest(configs.javascripts.dest));
  }

  if (configs.isDev) {
    bundler.on('update', function() {
      console.log('js done!');
      rebundle();
    });
  }

  rebundle();
})

gulp.task('watch', function() {
	gulp.watch(configs.stylesheets.watched,  ['scss']);
})

gulp.task('default', function() {
	gulp.run(defaultTasks);
	if (configs.isDev) gulp.run('watch');
})